var sound = new Howl({
    src: ['Blank.mp3']
});


function playSong() {
    sound.play();
}

function stopSong() {
    sound.stop();
}

function pauseSong() {
    sound.pause();
}



function updateTimer() {
    let currentTime = Math.trunc(sound.seek());
    document.getElementById('timer').innerHTML = currentTime.toString();
}


sound.on('play', setInterval(updateTimer, 1000));